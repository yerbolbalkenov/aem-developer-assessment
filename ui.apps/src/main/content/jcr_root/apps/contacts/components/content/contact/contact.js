use(["/libs/wcm/foundation/components/utils/ResourceUtils.js"], function (ResourceUtils) {

    var name1 = ResourceUtils.getResource(this.link + "/jcr:content/profile")
        .then(function (contentResource) {
            var name = contentResource.properties["name"];
            return name;
        });

    var title1 = ResourceUtils.getResource(this.link + "/jcr:content/profile")
        .then(function (contentResource) {
            var title = contentResource.properties["title"];
            return title;
        });

    var department1 = ResourceUtils.getResource(this.link + "/jcr:content/profile")
        .then(function (contentResource) {
            var department = contentResource.properties["department"];
            return department;
        });

    var position1 = ResourceUtils.getResource(this.link + "/jcr:content/profile")
        .then(function (contentResource) {
            var position = contentResource.properties["position"];
            return position;
        });

    var phone1 = ResourceUtils.getResource(this.link + "/jcr:content/profile")
        .then(function (contentResource) {
            var phoneArr = contentResource.properties["phone"];
            if (phoneArr.length){
                return phoneArr[0];
            }
            return undefined;
        });


    var email1 = ResourceUtils.getResource(this.link + "/jcr:content/profile")
        .then(function (contentResource) {
            var emailArr = contentResource.properties["email"];
            if(emailArr.length){
                return emailArr[0];
            }
            return undefined;
        });

    var address1 = ResourceUtils.getResource(this.link + "/jcr:content/profile")
        .then(function (contentResource) {
            var addressArr = contentResource.properties["address"];
            if(addressArr.length){
                return addressArr[0];
            }
            return undefined;
        });

    return {
        name: name1,
        title:title1,
        department: department1,
        position: position1,
        phone: phone1,
        email: email1,
        address: address1
        };
});

